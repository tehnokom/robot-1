/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "pwm_servo.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM15_Init();
  MX_ADC1_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  Servo_init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  ServoStart(180, 26);

  int servo_status = -1;
  int press_status = 0;
  uint32_t unbounce_B1 = HAL_GetTick();
  uint32_t retracting_time = HAL_GetTick();

  uint32_t dought_sensor;

  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 10);
  dought_sensor = HAL_ADC_GetValue(&hadc1);
  HAL_ADC_Stop(&hadc1);

  HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
  HAL_GPIO_WritePin(motor_reverce_GPIO_Port, motor_reverce_Pin, 0);

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


    switch(servo_status){
      case -1:
      	if(Servo1_is_reached() && Servo2_is_reached()){
      	    Servo1MoveTo(180, 200);
      	    Servo2MoveTo( 26, 200);
      	    servo_status++;
      	}
      	break;
      case 0:
	if(Servo1_is_reached() && Servo2_is_reached()){
	    Servo1MoveTo(180, 200);
	    Servo2MoveTo( 60, 200);
	    servo_status++;
	}
	break;
      case 1:
	if(Servo1_is_reached() && Servo2_is_reached()){
	    Servo1MoveTo(  0, 600);
	    Servo2MoveTo( 60, 400);
	    servo_status++;
	}
	break;
      case 2://Подъём струны
	if(Servo1_is_reached() && Servo2_is_reached()){
	    Servo1MoveTo( 20, 100);
	    Servo2MoveTo( 50, 100);
	    servo_status = 21;
	}
	break;
      case 21:
	if(Servo1_is_reached() && Servo2_is_reached()){
	    Servo1MoveTo( 40, 50);
	    Servo2MoveTo( 40, 50);
	    servo_status = 22;
	}
	break;
      case 22:
	if(Servo1_is_reached() && Servo2_is_reached()){
	    Servo1MoveTo( 50, 50);
	    Servo2MoveTo( 30, 50);
	    servo_status = 25;
	}
	break;
      case 25:
      	if(Servo1_is_reached() && Servo2_is_reached()){
      	    Servo1MoveTo( 50, 50);
      	    Servo2MoveTo( 26, 50);
      	    servo_status = 3;
      	}
      	break;
      case 3:
      	if(Servo1_is_reached() && Servo2_is_reached()){
      	    Servo1MoveTo(  0, 200);
      	    Servo2MoveTo( 23, 200);
      	    servo_status = 10;
      	}
      	break;
      case 4:
	if(Servo1_is_reached() && Servo2_is_reached()){
	  if(dought_sensor > 300){
	      servo_status ++;
	  }
	}
	break;
      case 5:
	if(Servo1_is_reached() && Servo2_is_reached()){
	    Servo1MoveTo(180, 200);
	    Servo2MoveTo( 23, 200);
	    servo_status++;
	}
	break;
      case 6:
	if(Servo1_is_reached() && Servo2_is_reached() && HAL_GPIO_ReadPin(right_limit_GPIO_Port, right_limit_Pin)){
	    servo_status = 1;
	}
	break;
      case 10://Ловушка для преостановки операций с ножом
	break;
      case 11://установка ножа в позицию для свободного снятия/установки контейнера с тестом
	if(Servo1_is_reached() && Servo2_is_reached()){
		    Servo1MoveTo(180, 200);
		    Servo2MoveTo( 50, 200);
		    servo_status = 10;
		}
      default:
	break;
    }

//=================================

    sConfig.Channel = ADC_CHANNEL_0;
    HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 10);
    dought_sensor = HAL_ADC_GetValue(&hadc1);
    HAL_ADC_Stop(&hadc1);
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, dought_sensor > 400);


    unsigned int up_limit;
    bool is_up_limit_reached(){
      return up_limit > 200;
    }

    sConfig.Channel = ADC_CHANNEL_1;
    HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 10);
    up_limit = HAL_ADC_GetValue(&hadc1);
    HAL_ADC_Stop(&hadc1);

    unsigned int down_limit;

    sConfig.Channel = ADC_CHANNEL_4;
    HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 10);
    down_limit = HAL_ADC_GetValue(&hadc1);
    HAL_ADC_Stop(&hadc1);


    switch (press_status) {
      case 0://Ожидаем нажатия кнопки
	if(!HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)){
	    unbounce_B1 = HAL_GetTick() + 50;
	    press_status++;
	}
	break;
      case 1://Проверяем дребезг
	if(HAL_GetTick() > unbounce_B1){
	    if(!HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)){
		press_status++;
	    }else{
		press_status--;
	    }
	}
	break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:// Запуск продавливания 11 порций
	if(servo_status == 10){
	    if(press_status != 12) // TODO: Последнюю итерацию нужно выделить отдельно
	      if( press_status != 2)
		servo_status = 4;
	      else
		servo_status = 5;
	    press_status++;
	}
	if(down_limit > 250){// TODO: Вынести задачи движения штока и обработки концевиков в отдельный автомат
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 1);
	    HAL_GPIO_WritePin(motor_reverce_GPIO_Port, motor_reverce_Pin, 1);
	}else{
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
	    servo_status = 11;
	    press_status = 20;
	}
	break;
      case 13:// Остановка перед запуском втягивания теста обратно
	retracting_time = HAL_GetTick() + 100;
	HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
	press_status++;
	break;
      case 14: // Запуск втягивания теста обратно
	if(HAL_GetTick() > retracting_time){
	    retracting_time = HAL_GetTick() + 5000;
	    HAL_GPIO_WritePin(motor_reverce_GPIO_Port, motor_reverce_Pin, 0);
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 1);
	    press_status++;
	}
	break;
      case 15:// Втягивания теста обратно
	if(HAL_GetTick() > retracting_time){
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
	    press_status++;
	}else if(is_up_limit_reached()){ // Втягивать больше некуда
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
	    press_status++;
	}
	break;
      case 16:
	HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
	HAL_GPIO_WritePin(motor_reverce_GPIO_Port, motor_reverce_Pin, 0);
	press_status = 0;
	break;
      case 20:// Закончилось тесто -- возврат штока
	if(up_limit < 200){
	    HAL_GPIO_WritePin(motor_reverce_GPIO_Port, motor_reverce_Pin, 0);
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 1);
	}else{
	    HAL_GPIO_WritePin(motor_reverce_GPIO_Port, motor_reverce_Pin, 1);
	    HAL_GPIO_WritePin(motor_on_GPIO_Port, motor_on_Pin, 0);
	    press_status = 0;
	}
	break;
      default:
	press_status = 0;
	break;
    }

    char str[100];
    sprintf(str, "\rDought: %6d  Up: %6d  Down: %6d Servo: %2d Press: %2d ",
	    dought_sensor, up_limit, down_limit, servo_status, press_status);
    HAL_UART_Transmit(&huart2, str, strlen(str), 10);


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* TIM15_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM15_IRQn);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
